//
//  TAFmenuViewController.h
//  TakeAwayFood
//
//  Created by Carmen Maria Morillo Arias on 18/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAFRestaurante.h"
#import "TAFconnection.h"

@interface TAFmenuViewController : UITableViewController

@property (nonatomic,strong) TAFRestaurante *restaurante;

@property (nonatomic,strong) NSDecimalNumber *idRestaurante;


@end
