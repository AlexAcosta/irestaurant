//
//  TAFdetalleViewController.h
//  TakeAwayFood
//
//  Created by Máster INFTEL 07 on 21/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAFPlato.h"
#import "TAFLineaPedido.h"

@interface TAFdetalleViewController : UIViewController


@property IBOutlet UIImageView *imagen;
@property IBOutlet UILabel *nombre;
@property IBOutlet UITextView *descripcion;
@property IBOutlet UILabel *precio;

@property (nonatomic,strong) NSDecimalNumber *plato;
@property IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *txfReference;
@property BOOL keyboardIsShown;
@property BOOL displaykeyboard;
@property int kKeyboardAnimationDuration;
@property CGPoint offset;
@property int screenWidth;
- (void) refrescarVista;

@end
