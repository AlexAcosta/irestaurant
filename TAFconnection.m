//
//  TAFconnection.m
//  TakeAwayFood
//
//  Created by Máster INFTEL 06 on 19/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
// 192.168.183.55

#import "TAFconnection.h"


@implementation TAFconnection


+(void) connectionUsuarioNombre: (NSString *)nombre pass: (NSString *)pass label: (UILabel *) sms botonentrar: (UIButton *)entrarbutton botonlogin: (UIButton *)loginbutton load: (UIActivityIndicatorView *)loading{
    
    
    /*NSURL *url = [NSURL URLWithString:[[[@"http://192.168.1.40:8080/AlexAcosta-webservicejava-a432e0aa02a8/webresources/jpa.usuario/name=" stringByAppendingString:nombre]stringByAppendingString: @"&pass="] stringByAppendingString:pass ]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];*/
    NSURL *url = [NSURL URLWithString:[[[@"http://192.168.183.55:8080/ResfultOtroMas/webresources/jpa.usuario/name=" stringByAppendingString:nombre]stringByAppendingString: @"&pass="] stringByAppendingString:pass ]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *greeting = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:NULL];
             if([greeting count]>0){
                 TAFAppDelegate *appdelegate = [[TAFAppDelegate alloc] init];
                 NSManagedObjectContext *context = appdelegate.managedObjectContext;
                 TAFUsuario *usuario= [TAFUsuario usuarioWithContext:context];
                // [appdelegate deleteAllObjects:@"TAFUsuario"];
                 id value;
                 
                 
                 value = [greeting objectForKey: @"telefono"];
                 [usuario setTelefono: value];
                 value = [greeting objectForKey: @"contrasena"];
                 [usuario setPass: value];
                 value = [greeting objectForKey: @"direccion"];
                 [usuario setDireccion: value];
                 value = [greeting objectForKey: @"nombreUsuario"];
                 [usuario setNombreusuario: value];
                 value = [greeting objectForKey: @"idUsuario"];
                 [usuario setId: value];
                 [appdelegate setidusu:value];
                 
                 
                 
                 [appdelegate saveContext];
                 [sms setTextColor:[UIColor greenColor]];
                 [sms setText:@"Estas conectado"];
                 [entrarbutton setHidden:NO];
                 [loginbutton setHidden:YES];
                 
                 [loading stopAnimating];
                 [loading setHidden:YES];
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 
             }else{
                 [sms setTextColor:[UIColor redColor]];
                 [sms setText:@"Nombre o Password incorrecto"];
                 [entrarbutton setHidden:YES];
                 [loginbutton setHidden:NO];
                 [loading stopAnimating];
                 [loading setHidden:YES];
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 
             }
             
         }
         
     }];
    
    
    
}

+(void) connectionRestauranteLatitud: (float ) lat longitud: (float )longitud restaurantes: (NSMutableArray *) restaurantes tableview:(UITableView *) tableview load: (UIActivityIndicatorView *)loading{
    
    
    
    [loading startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
      /*  NSURL *url = [NSURL URLWithString:[[[@"http://192.168.1.40:8080/AlexAcosta-webservicejava-a432e0aa02a8/webresources/jpa.restaurante/latitud=" stringByAppendingString: [NSString stringWithFormat:@"%0.8f",lat]]stringByAppendingString: @"&longitud="]stringByAppendingString: [NSString stringWithFormat:@"%0.8f",longitud]]];*/
    NSURL *url = [NSURL URLWithString:[[[@"http://192.168.183.55:8080/ResfultOtroMas/webresources/jpa.restaurante/latitud=" stringByAppendingString: [NSString stringWithFormat:@"%0.8f",lat]]stringByAppendingString: @"&longitud="]stringByAppendingString: [NSString stringWithFormat:@"%0.8f",longitud]]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         
         if (data.length > 0 && connectionError == nil)
         {
             NSArray *greeting = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
             
             id value;
             
             for(NSDictionary *dic in greeting){
                 TAFAppDelegate *appdelegate = [[TAFAppDelegate alloc] init];
                 NSManagedObjectContext *context = appdelegate.managedObjectContext;
                 TAFRestaurante *restaurante= [TAFRestaurante restauranteWithContext:context];
               //  [appdelegate deleteAllObjects:@"TAFRestaurante"];
                 value = [dic objectForKey: @"nombreRestaurante"];
                 [restaurante setNombre: value];
                 [restaurantes addObject:value];
                 value = [dic objectForKey: @"idRestaurante"];
                 [restaurante setId: value];
                 [appdelegate saveContext];
                 
                 [loading stopAnimating];
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 [loading setHidden:YES];
             }
             
             
             [tableview reloadData];
         }
     }];
    
    
}

+(void) connectionRestaurantePlatos: (NSDecimalNumber *) restaurante  platos: (NSMutableDictionary *) platos tableview:(UITableView*) tableview load: (UIActivityIndicatorView *)loading{
    
 /*   NSURL *url = [NSURL URLWithString:[@"http://192.168.1.40:8080/AlexAcosta-webservicejava-a432e0aa02a8/webresources/jpa.platos/restaurante=" stringByAppendingString: [NSString stringWithFormat:@"%@",restaurante]]];*/
    
    NSURL *url = [NSURL URLWithString:[@"http://192.168.183.55:8080/ResfultOtroMas/webresources/jpa.platos/restaurante=" stringByAppendingString: [NSString stringWithFormat:@"%@",restaurante]]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         
         if (data.length > 0 && connectionError == nil)
         {
             NSError *error;
             
             NSArray *resultado = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
             
             NSLog(@"Error: %@",error);
             
             for(NSDictionary *dic in resultado){
                 
                 NSString *tipo;
                 NSString *nombrePlato;
                 
                 TAFAppDelegate *appdelegate = [[TAFAppDelegate alloc] init];
                 NSManagedObjectContext *context = appdelegate.managedObjectContext;
                 TAFPlato *plato= [TAFPlato platoWithContext:context];
               //   [appdelegate deleteAllObjects:@"TAFPlato"];
                 
                 [appdelegate setidrest:restaurante];
                 nombrePlato = [dic objectForKey: @"nombrePlato"];
                 [plato setNombre:nombrePlato];
                 
                 tipo = [[dic objectForKey:@"idTipo"] objectForKey:@"nombreTipo"];
                 
                 [plato setId:[dic objectForKey:@"idPlato"]];
                 
                 [plato setPrecio:[dic objectForKey:@"precio"]];
                 
                 [platos setValue:tipo forKey:nombrePlato];
                 
                 [appdelegate saveContext];
                 [loading stopAnimating];
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 [loading setHidden:YES];
             }
             [tableview reloadData];
         }
     }];
}
+(void) connectionDetallesPlato: (NSDecimalNumber *) idplato detalleView:(TAFdetalleViewController *) detalleView load: (UIActivityIndicatorView *)loading{
    
    
   /* NSURL *url = [NSURL URLWithString: [@"http://192.168.1.40:8080/AlexAcosta-webservicejava-a432e0aa02a8/webresources/jpa.platos/" stringByAppendingString: [NSString stringWithFormat:@"%@",idplato]]];*/
    
    NSURL *url = [NSURL URLWithString: [@"http://192.168.183.55:8080/ResfultOtroMas/webresources/jpa.platos/" stringByAppendingString: [NSString stringWithFormat:@"%@",idplato]]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *resultado = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
             
             [detalleView.descripcion setText:[resultado objectForKey: @"descripcion"]];
             
             NSData *dat = [[NSData alloc] initWithData:[NSData dataFromBase64String:[resultado objectForKey: @"foto"]]];
             UIImage *imagedes = [UIImage imageWithData:dat];
             [detalleView.imagen setImage:imagedes];
             
             [detalleView.nombre setText:[resultado  objectForKey:@"nombrePlato"]];
             
             [detalleView.precio setText:[NSString stringWithFormat:@"%@ €",[resultado objectForKey: @"precio"]]];
             
             
             [loading stopAnimating];
             [loading setHidden:YES];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             
             [detalleView refrescarVista];
         }
     }];
}


@end
