//
//  TAFdetalleViewController.m
//  TakeAwayFood
//
//  Created by Máster INFTEL 07 on 21/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
//

#import "TAFdetalleViewController.h"
#import "NSData+Base64.h"
#import "TAFconnection.h"

@interface TAFdetalleViewController ()

@property NSDecimalNumber *idplt;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;

@end

@implementation TAFdetalleViewController
@synthesize txfReference;
@synthesize keyboardIsShown;
@synthesize displaykeyboard;
@synthesize kKeyboardAnimationDuration;
@synthesize offset;
@synthesize screenWidth;
@synthesize plato;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.descripcion addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    self.idplt=plato;
    
    [TAFconnection connectionDetallesPlato:plato detalleView:self load:self.loading];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    screenWidth=[[UIScreen mainScreen] bounds].size.width;
    
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refrescarVista
{
    [self reloadInputViews];
}

- (IBAction)anhadir
{
    TAFAppDelegate *appdelegate = [[TAFAppDelegate alloc] init];
    NSFetchRequest *request =[[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = appdelegate.managedObjectContext;
    NSError *error;
    [request setEntity:[NSEntityDescription entityForName:@"TAFLineaPedido" inManagedObjectContext:context]];
    NSLog(@"La puñetera id%@",self.idplt);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", self.idplt];
    [request setPredicate:predicate];
    
    NSArray *items =[context executeFetchRequest:request error:&error];
    
    //le pasamos el restaurante
    if([items count]>0){
        
        TAFLineaPedido *lpant=[items objectAtIndex:0];
       /* [lpant setId: lpant.id];
        [lpant setNombre: lpant.nombre];
        [lpant setPrecio: lpant.precio];*/
        NSNumber *newcant=[NSNumber numberWithInt:(lpant.cantidad.intValue +1)];
        [lpant setCantidad:newcant];
        
        
        [appdelegate saveContext];
    }else{
        TAFLineaPedido *lpnew= [TAFLineaPedido lineaWithContext:context];
        
        [lpnew setId: self.idplt];
        [lpnew setNombre: self.nombre.text];
        
        NSNumberFormatter * f =[[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterNoStyle];
        
        [lpnew setPrecio:[NSDecimalNumber decimalNumberWithString:self.precio.text]];
        NSNumber *newcant=[NSNumber numberWithInt:1];
        [lpnew setCantidad:newcant];
        
        
        [appdelegate saveContext];
    }
    
    NSFetchRequest *fecthRequest =[[NSFetchRequest alloc] init];
    
    NSEntityDescription *usuario= [NSEntityDescription entityForName:@"TAFLineaPedido" inManagedObjectContext:context];
    
    [fecthRequest setEntity:usuario];
    
    
    NSArray *itemsi =[context executeFetchRequest:fecthRequest error:&error];
    
    NSLog(@"PEDIDO TIULTOOOO");
    for(TAFLineaPedido *hola in itemsi){
        NSLog(@"PEDIDO");
        NSLog(@"Venga va %@",[hola nombre]);
        NSLog(@"Venga va %@",[hola cantidad]);
              NSLog(@"Venga va %@",[hola precio]);
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Aviso:" message:@"Plato añadido correctamente" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    txfReference = textField;
}

-(void) keyboardDidShow: (NSNotification *) notif
{
    if (displaykeyboard) {
        return;
    }
    
    NSDictionary *info = [notif userInfo];
    NSValue *aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    int heightTab = 49;
    
    offset = self.scrollView.contentOffset;
    
    CGRect viewFrame;
    
    /*if (bIsTimeQuestion) {
     viewFrame = CGRectMake(kXScroll, kYScroll + kHeightBar, 320, [self isiPhoneFourInchScrolls] - kHeightBar);
     }else
     {*/
    int heightScreen = [[UIScreen mainScreen] bounds].size.height;
    
    
    viewFrame = CGRectMake(0, 0, screenWidth,heightScreen);
    //}
    
    viewFrame.size.height -= keyboardSize.height;
    self.scrollView.frame = viewFrame;
    CGRect TextFieldRect = [txfReference frame];
    TextFieldRect.origin.y += 10;
    [self.scrollView scrollRectToVisible:TextFieldRect animated:YES];
    [self.scrollView setFrame:CGRectMake(0, 0, screenWidth, heightScreen - keyboardSize.height)];
    [self.scrollView setContentSize:CGSizeMake(screenWidth, heightScreen - heightTab)];
    
    displaykeyboard = YES;
}

-(void) keyboardDidHide:(NSNotification *) notif
{
    if (!displaykeyboard) {
        return;
    }
    
    
    //NSDictionary *info = [notif userInfo];
    //NSValue *aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    
    offset = self.scrollView.contentOffset;
    
    CGRect viewFrame;
    
    int heightScreen = [[UIScreen mainScreen] bounds].size.height;
    
    viewFrame = CGRectMake(0, 0, screenWidth,heightScreen);
    self.scrollView.frame = viewFrame;
    CGRect TextFieldRect = [txfReference frame];
    TextFieldRect.origin.y += 10;
    [self.scrollView scrollRectToVisible:TextFieldRect animated:YES];
    [self.scrollView setFrame:CGRectMake(0, 0, screenWidth, heightScreen)];
    [self.scrollView setContentSize:CGSizeMake(screenWidth, heightScreen)];
    
    displaykeyboard = NO;
}


@end
