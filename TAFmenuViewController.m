//
//  TAFmenuViewController.m
//  TakeAwayFood
//
//  Created by Carmen Maria Morillo Arias on 18/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
//

#import "TAFmenuViewController.h"
#import "TAFPlato.h"
#import "TAFTipo.h"
#import "TAFconnection.h"
#import "TAFdetalleViewController.h"


@interface TAFmenuViewController ()

@property NSMutableDictionary *platos;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loading;

@end

@implementation TAFmenuViewController {
    NSArray *tipos;
}

@synthesize restaurante;
@synthesize platos;
@synthesize idRestaurante;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    platos = [[NSMutableDictionary alloc] init];
    [[self loading] setHidden:NO];
    [self.loading startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    NSLog(@"%@", idRestaurante);
    
    //Inicializamos los tipos
    tipos = [NSArray arrayWithObjects:@"Entrantes", @"Carnes", @"Pescados", @"Postres", @"Bebidas", nil];
    
   [TAFconnection connectionRestaurantePlatos:idRestaurante platos:platos tableview:self.tableView load:self.loading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [tipos count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = 0;
    for (id key in platos) {
        NSString *tipo = [platos objectForKey:key];
        
        if ([tipo isEqualToString:tipos[section]]) {
            count++;
        }
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"platoCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"platoCell"];
    }
    
    NSMutableArray *platosPorTipo = [[NSMutableArray alloc] init];
    
    NSString *tipoSeccion = [tipos objectAtIndex:indexPath.section];
    
    for (id key in platos) {
        NSString *tipo = [platos objectForKey:key];
        
        if ([tipo isEqualToString:tipoSeccion]) {
            [platosPorTipo addObject:key];
        }
    }
    
    [cell.textLabel setText:[platosPorTipo objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [tipos objectAtIndex:section];
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    //Comprobamos que es el identificador correcto
    if ([segue.identifier isEqualToString:@"detallesPlato"]) {
        
        //Obtenemos la fila pulsada
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        //Obtenemos el controller de destino
        TAFdetalleViewController *detallesViewController = segue.destinationViewController;
        
        //Obtenemos el plato seleccionado
        UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        //Obtenemos el plato del core data
        TAFAppDelegate *appdelegate = [[TAFAppDelegate alloc] init];
        NSFetchRequest *request =[[NSFetchRequest alloc] init];
        NSManagedObjectContext *context = appdelegate.managedObjectContext;
        NSError *error;
        [request setEntity:[NSEntityDescription entityForName:@"TAFPlato" inManagedObjectContext:context]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"nombre == %@", selectedCell.textLabel.text];
        [request setPredicate:predicate];
        
        NSArray *items =[context executeFetchRequest:request error:&error];
        
        TAFPlato *plato = [items objectAtIndex:0];
        
        //Le pasamos el plato a la pantalla de detalles
        detallesViewController.plato = plato.id;
    }
    
}



@end



