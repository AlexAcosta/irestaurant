//
//  TAFconnection.h
//  TakeAwayFood
//
//  Created by Máster INFTEL 06 on 19/01/14.
//  Copyright (c) 2014 INFTEL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAFUsuario.h"
#import "TAFAppDelegate.h"
#import "TAFRestaurante.h"
#import "NSData+Base64.h"
#import "TAFdetalleViewController.h"
#import "TAFplato.h"


@interface TAFconnection : NSObject

+(void) connectionUsuarioNombre: (NSString *)nombre pass: (NSString *)pass label: (UILabel *) sms botonentrar: (UIButton *)entrarbutton botonlogin: (UIButton *)loginbutton load: (UIActivityIndicatorView *)loading;


+(void) connectionRestauranteLatitud: (float ) lat longitud: (float )longitud restaurantes: (NSMutableArray *) restaurantes tableview:(UITableView *) tableview load: (UIActivityIndicatorView *)loading;

+(void) connectionRestaurantePlatos: (NSDecimalNumber *) restaurante  platos: (NSMutableDictionary *) platos tableview:(UITableView*) tableview load: (UIActivityIndicatorView *)loading;

+(void) connectionDetallesPlato: (NSDecimalNumber *) plato detalleView:(TAFdetalleViewController *) detalleView load: (UIActivityIndicatorView *)loading;

@end
